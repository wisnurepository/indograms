<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use Auth;
use Storage;
use Carbon\Carbon;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $posts = Post::leftJoin('users','users.id','=','posts.user_id')
                     ->get([
                       'posts.id',
                       'posts.picture',
                       'posts.detail',
                       'users.email',
                       'users.name',
                       'posts.created_at'
                     ]);
        // dd($posts);
        return view('dashboard',compact('posts'));
    }

    public function postWorld(Request $request)
    {
      $new =  new Post;      
      $gambarpath = 'Picture/' . Auth::user()->id . '/Images';
      $dataImage = $this->upload('Images' ,   $request->image, $gambarpath);
      $new->user_id = Auth::user()->id;
      $new->detail = $request->detail;
      $new->picture = $dataImage;
      $new->save();

      $response = ['data' => 'Ok'];

      return response()->json($response);
      
    }

    public function upload($column, $request, $store_path)
    {
      $file = $request;
      $filename = Carbon::now()->format('Y-m-d-H-i-s'). $column . '.' . $file->getClientOriginalExtension();
      $path = $file->storeAs($store_path, $filename, 'public');
      return $path;

    }

    public function detailPost($id)
    {
      $post = Post::leftJoin('users','users.id','=','posts.user_id')->where('posts.id',$id)->first();
      
      return view('detail',compact('post'));
    }
}
