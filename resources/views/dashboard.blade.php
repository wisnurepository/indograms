@extends('layouts.app')

@section('content')
<nav class="navbar navbar-light bg-white">
  <a href="#" class="navbar-brand">Indograms</a>
  <div class="form-inline">
      <div class="form-control">
      <form method="POST" action="{{ route('logout') }}">
        @csrf
        
        <button class="btn btn-outline-danger" type="submit">
            <i class="fa fa-lock"></i>
        </button>
      </form>
      </div>
    </div>
</nav>


<div class="container-fluid gedf-wrapper">
  <div class="row">
      <div class="col-md-3">
          <div class="card">
              <div class="card-body">
              <div class="h5">{{ucfirst(Auth::user()->name)}}</div>
                  <div class="h7 text-muted">E-mail : {{Auth::user()->email}}</div>
                  <div class="h7">Indograms Indonesia
                  </div>
              </div>
          </div>
      </div>
      <div class="col-md-6 gedf-main">

          <!--- \\\\\\\Post-->
          <div class="card gedf-card mb-3">
              <div class="card-header">
                  <ul class="nav nav-tabs card-header-tabs" id="myTab" role="tablist">
                      <li class="nav-item">
                          <a class="nav-link active" id="posts-tab" data-toggle="tab" href="#posts" role="tab" aria-controls="posts" aria-selected="true">Make
                              a World</a>
                      </li>
                  </ul>
              </div>
              <div class="card-body">
                <form id="formPost" method="post" action="javascript:void(0)" enctype="multipart/form-data">
                  @csrf 
                  <div class="tab-content" id="myTabContent">
                      <div class="tab-pane fade show active" id="posts" role="tabpanel" aria-labelledby="posts-tab">
                        <div class="form-group">
                            <input type="file" name="image" id="customFile" required>
                        </div>
                        <div class="form-group">
                            <label class="sr-only" for="message">post</label>
                            <textarea class="form-control" id="message" name="detail" rows="3" placeholder="What are you thinking?" required></textarea>
                        </div>
                      </div>
                  </div>
                  <div class="btn-toolbar float-right">
                      <div class="btn-group">
                          <button type="submit" class="btn btn-primary">share</button>
                      </div>
                  </div>

                  </form>
              </div>
          </div>
          <!-- Post /////-->

          <!--- \\\\\\\Post-->
          @if($posts)
          @foreach($posts as $post)
          <div class="card gedf-card mb-3">
              <div class="card-header">
                  <div class="d-flex justify-content-between align-items-center">
                      <div class="d-flex justify-content-between align-items-center">
                          <div class="mr-2">
                              <img class="rounded-circle" width="45" src="https://picsum.photos/50/50" alt="">
                          </div>
                          <div class="ml-2">
                            <div class="h5 m-0">{{$post->name}}</div>
                              <div class="h7 text-muted">{{$post->email}}</div>
                          </div>
                      </div>
                  </div>

              </div>
              <div class="card-body">
                  <div class="text-muted h7 mb-2"> <i class="fa fa-clock-o"></i> {{$post->created_at->format('d-m-Y')}}</div>
                <a class="card-link" href="/detailPost/{{$post->id}}">
                  <img src="{{Storage::url($post->picture)}}"class="img-responsive" style="max-width:70%;" alt="">
                </a>

                  <p class="card-text mt-2">
                    {{$post->detail}}
                  </p>
              </div>
          </div>

          @endforeach
          @else
          <div class="card gedf-card mb-3">
              <div class="card-header">
                  <div class="d-flex justify-content-between align-items-center">
                      <div class="d-flex justify-content-between align-items-center">
                          <div class="mr-2">
                              <img class="rounded-circle" width="45" src="https://picsum.photos/50/50" alt="">
                          </div>
                          <div class="ml-2">
                              <div class="h5 m-0">@LeeCross</div>
                              <div class="h7 text-muted">Miracles Lee Cross</div>
                          </div>
                      </div>
                  </div>

              </div>
              <div class="card-body">
                  <div class="text-muted h7 mb-2"> <i class="fa fa-clock-o"></i> 10 min ago</div>
                  <a class="card-link" href="#">
                      <h5 class="card-title">Lorem ipsum dolor sit amet, consectetur adip.</h5>
                  </a>
                  <img src="https://picsum.photos/870/500"class="img-responsive" alt="">

                  <p class="card-text mt-2">
                      Lorem ipsum dolor sit amet consectetur adipisicing elit. Quo recusandae nulla rem eos ipsa praesentium esse magnam nemo dolor
                      sequi fuga quia quaerat cum, obcaecati hic, molestias minima iste voluptates.
                  </p>
              </div>
          </div>

          @endif
          <!-- Post /////-->





      </div>
  </div>
</div>

<script type="text/javascript">
  $(document).ready(function () {
    $.ajaxSetup({
    headers: {
            'X-CSRF-TOKEN': $('meta[name="token"]').attr('content')
        }
    });

    $('#formPost').submit(function(e){
    e.preventDefault();    
    var formData = new FormData(this);

    $.ajax({
        url: '/postWorld',
        type:'POST',
        data: formData,
        success: function (data) {
          console.log(data.data);
          if(data.data == 'Ok')
          {
            window.location = '/home';
          }
        },
        cache: false,
        contentType: false,
        processData: false
    });
    })
  
  })

</script>
@endsection