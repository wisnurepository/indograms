@extends('layouts.app')

@section('content')
<nav class="navbar navbar-light bg-white">
  <a href="/home" class="navbar-brand">Indograms</a>
  <div class="form-inline">
      <div class="form-control">
      <form method="POST" action="{{ route('logout') }}">
        @csrf
        
        <button class="btn btn-outline-danger" type="submit">
            <i class="fa fa-lock"></i>
        </button>
      </form>
      </div>
    </div>
</nav>


<div class="container-fluid gedf-wrapper">
  <div class="row">
      <div class="col-md-3">
      </div>
      <div class="col-md-6 gedf-main">
          <!--- \\\\\\\Post-->
          <div class="card gedf-card mb-3">
              <div class="card-header">
                <a href="/home" class="float-right"> Back</a>
                  <div class="d-flex justify-content-between align-items-center">
                      <div class="d-flex justify-content-between align-items-center">
                          <div class="mr-2">
                              <img class="rounded-circle" width="45" src="https://picsum.photos/50/50" alt="">
                          </div>
                          <div class="ml-2">
                            <div class="h5 m-0">{{$post->name}}</div>
                              <div class="h7 text-muted">{{$post->email}}</div>
                          </div>
                      </div>
                  </div>

              </div>
              <div class="card-body">
                  <div class="text-muted h7 mb-2"> <i class="fa fa-clock-o"></i> {{$post->created_at->format('d-m-Y')}}</div>
                <a class="card-link" href="/postDetail/{{$post->id}}">
                  <img src="{{Storage::url($post->picture)}}"class="img-responsive" style="max-width:70%;" alt="">
                </a>

                  <p class="card-text mt-2">
                    {{$post->detail}}
                  </p>
              </div>
          </div>

          <!-- Post /////-->
      </div>
  </div>
</div>

@endsection